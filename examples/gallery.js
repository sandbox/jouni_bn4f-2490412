/**
 * @file
 * Generating the chart pickers for real data sets.
 */
 
/**
 * Build gallery of charts for chart picker.
 */
function nvd3Charter(myID) {

  // For debug of XML demo files = true.
  var xml = false;

  // All valid names of chart types.
  var types = ['simpleline', 'lineplusbar', 'scatterbubble', 'viewfinder',
    'multibar', 'cumulativeline', 'stackedarea', 'discretebar',
    'horizontalmultibar', 'donut', 'pie', 'bullet'];

  var pcs = types.length;
  var tabs = '';
  var charts = '';

  for (i = 0; i < pcs; i++) {
    var myChart = '<span id="chart' + i + '">Empty Chart (' + (i + 1) +
      ') Container</span>';
    tabs = tabs + '<li><a href="#tabs-' + i + '" style="font-size:10px">' +
      types[i] + '</a></li>';
    charts = charts + '<div id="tabs-' + i + '">' + myChart + '</div>';
  }

  var buildTabs =
    '<script> jQuery(function() { jQuery( "#tabs" ).tabs(); }); </script>';

  var msg =
    "Select here your chart's basic type, its data source, and where new chart can show up.<br />";
  var res = msg + buildTabs + '<div id="tabs"><ul>' + tabs + '</ul>' + charts +
    '</div>';

  // Print out chart's container.
  jQuery("#" + myID).html(res);

  var h = 350;
  var w = 850;

  // Generate all types of charts.
  for (i = 0; i < pcs; i++) {
    var timeaxis = false;
    if (types[i] == 'cumulativeline' || types[i] == 'stackedarea' || types[i] ==
      'lineplusbar') {
      timeaxis = true;
    }
    jsChart(i, '', types[i], {
      height: h,
      width: w
    }, {
      xtime: timeaxis,
      showLegend: true,
      xmldemo: xml,
      shadows: 'black',
      backgroundcolor: 'darkgray'
    });
  }
  return;
}

/**
 * Old gallery of charts for chart picker.
 *
 * HTML table format.
 */
function nvd3Demos(myID, pcs, xmldemo) {

  // XML versions of demos asked or not.
  var xml = false;
  if (xmldemo) {
    xml = true;
  }

  // Generate empty table for envelope.
  var html = '';
  for (i = 0; i < pcs; i++) {
    html = html + '<tr><td id="chart' + i + '">Empty Chart (' + (i + 1) +
      '.) Container</td></tr>';
  }
  html = '<table>' + html + '</table>';

  // Print it out into chart's container.
  jQuery("#" + myID).html(html);

  /*
  All valid names of chart types

  simpleline  linePlusBar  scatterbubble  viewfinder  multibar  multibar
  cumulativeline  stackedarea  discretebar  horizontalmultibar  donut  pie  bullet
   */

  pcs--;
  jsChart(pcs, '', 'linePlusBar', {
    height: '250',
    width: '350'
  }, {
    showLegend: true,
    tooltips: true,
    transitionDuration: 3000,
    useInteractiveGuideline: true,
    xtime: true,
    xmldemo: xml,
    shadows: 'black',
    backgroundcolor: 'SandyBrown'
  });
  pcs--;

  jsChart(pcs, '', 'bullet', {
    height: '250',
    width: '300'
  }, {
    showLegend: true,
    xmldemo: xml
  });
  pcs--;

  jsChart(pcs, '', 'simpleline', {
    height: '250',
    width: '300'
  }, {
    showLegend: false,
    transitionDuration: 3000,
    xmldemo: xml,
    shadows: 'black',
    domain: {
      minY: -1.5,
      maxY: 2
    }
  });
  pcs--;

  jsChart(pcs, '', 'scatterbubble', {
    height: '250',
    width: '300'
  }, {
    showLegend: true,
    xmldemo: xml,
    shadows: 'black'
  });
  pcs--;

  jsChart(pcs, '', 'viewfinder', {
    height: '250',
    width: '300'
  }, {
    showLegend: true,
    xmldemo: xml,
    shadows: 'black'
  });
  pcs--;

  jsChart(pcs, '', 'multibar', {
    height: '250',
    width: '300'
  }, {
    showLegend: false,
    xmldemo: xml,
    shadows: 'black'
  });
  pcs--;

  jsChart(pcs, '', 'cumulativeline', {
    height: '250',
    width: '300'
  }, {
    showLegend: false,
    xmldemo: xml,
    xtime: true,
    shadows: 'black',
    backgroundcolor: 'SkyBlue'
  });
  pcs--;

  var bg_pict = rootpath + '../backgrounds/';
  jsChart(pcs, '', 'stackedarea', {
    height: '250',
    width: '300'
  }, {
    showLegend: false,
    xmldemo: xml,
    xtime: true,
    shadows: 'black',
    backgroundimage: bg_pict + 'continents1.jpg'
  });
  pcs--;

  jsChart(pcs, '', 'discretebar', {
    height: '250',
    width: '300'
  }, {
    color: ['red', 'green', 'blue', 'orange', 'brown', 'navy', 'yellow',
      'black'],
    xmldemo: xml,
    shadows: 'black',
    backgroundimage: bg_pict + 'continents11.jpg',
    minY: -200
  });
  pcs--;

  jsChart(pcs, '', 'horizontalmultibar', {
    height: '250',
    width: '450'
  }, {
    showLegend: false,
    xmldemo: xml,
    shadows: 'black'
  });
  pcs--;

  jsChart(pcs, '', 'donut', {
    height: '250',
    width: '300'
  }, {
    showLegend: true,
    xmldemo: xml,
    shadows: 'black'
  });
  pcs--;

  jsChart(pcs, '', 'pie', {
    height: '250',
    width: '300'
  }, {
    showLegend: true,
    xmldemo: xml,
    shadows: 'black'
  });

}

/**
 * Old gallery of charts for chart picker.
 *
 * HTML table format for WordPress & Drupal 7.
 *
 */
function demoShows(id, data, type, options) {

  // Demo data sets for gallery.
  var demos = {
    lineplusbar: 'linePlusBarData.json',
    simpleline: 'simpleLineData.json',
    cumulativeline: 'cumulativeLineData.json',
    stackedarea: 'stackedAreaData.json',
    discretebar: 'discreteBarData.json',
    horizontalmultibar: 'multibarData.json',
    pie: 'pieData.json',
    donut: 'pieData.json',
    bullet: 'bulletData.json',
    scatterbubble: 'scatterData.json',
    multibar: 'multiData.json',
    viewfinder: 'viewFinderData.json'
  };

  if (options.xmldemo) {
    demos[type] = demos[type].replace(/json/g, 'xml');
  }

  // Home dir of demo data sets.
  var infile = 'wp-content/plugins/nvd3/data/' + demos[type];
  // Global URL of root set by shortcode of WP.
  if (rootpath) {
    infile = rootpath + demos[type];
  }

  var desc = 'Data File: data/' + demos[type];
  var subs = '<sup> ?</sup>';
  var msg = '<b class="title_nvd3" title="' + desc + '">Chart Type: ' + type +
    subs + '</b>';
  msg = '<br /><a href="' + infile + '" target="_blank">' + msg + '</a>';

  var ctype = '&type=' + type;
  var filepath = '&filepath=' + demos[type];
  var tt = 'Clone data set from this chart into new draft for preview.';

  var shortmsg = '<br />Add this into: ';

  var qcms = '';
  // WordPress CMS publications is default.
  var mpostpage =
    '<option value="post">New Post</option><option value="page">New Page</option>';
  if (cms) {
    if (cms == 'drupal') {
      mpostpage =
        '<option value="post">New Article</option><option value="page">New Basic Page</option>';
      qcms = '&cms=' + cms;
    }
  }
  var idmenu = "gmenu" + id;
  mpostpage = '<select id=' + idmenu + '>' + mpostpage + '</select>';

  var helps = new Array();
  helps.push('Data input from JSON file');
  helps.push('Data input from XML file');
  helps.push('Data input from CSV file');
  helps.push('Data input from TSV file');
  helps.push('Data input from direct JSON: values, labels, series, and links');
  helps.push('Data input from HTML of page');
  helps.push('Data input from simple table (OpenOffice compatible)');
  helps.push('Data input from 2x2 table (OpenOffice compatible)');
  var idmenu2 = "gformat" + id;
  var mformat = '<option value="json" title="' + helps[0] +
    '">JSON data</option><option value="xml" title="' + helps[1] +
    '">XML data</option><option value="csv" title="' + helps[2] +
    '">CSV data</option><option value="tsv" title="' + helps[3] +
    '">TSV data</option><option value="direct" title="' + helps[4] +
      '">Direct input</option><option value="cells" title="' + helps[5] +
      '">Content of text</option>';
  /* @todo
  mformat = mformat + '<option value="table" title="' + helps[6] +
    '">Document TABLE</option><option value="table2" title="' + helps[7] +
    '">TABLE (2x2 dim.)</option>';
  */
  mformat = '<select id=' + idmenu2 + '>' + mformat + '</select>';

  var query = rootpath + "../postchart.php?type=" + type + qcms;
  var ctype = demos[type];
  var mbutt = '<button style="cursor:pointer" onclick="newpost2(' + sQuote(
    query) + ', ' + sQuote(ctype) + ', ' + sQuote(idmenu) + ', ' + sQuote(
    idmenu2) + ')" title="' + tt + '">New Chart</button>'

  var aform = shortmsg + mpostpage + ' from ' + mformat + mbutt;

  if (infile.indexOf(".json") > 0) {
    d3.json(infile, function(error, data) {
      chartSelector(id, data, type, options);
      console.info('Drawing chart "' + type + '" from a file: data/' +
        demos[type]);
      jQuery("#chart" + id).append(msg + aform);
    });
  }
  else if (infile.indexOf('.xml') > 0) {
    // This d3.xml had its parsing problems of xml: read this as  a text.
    d3.text(infile, function(error, data) {
      // Old one: data = buildXML(data);
      data = xml2json(data, '  ');
      chartSelector(id, data, type, options);
      console.info('Drawing chart "' + type + '" from a XML file: data/' +
        demos[type]);
      jQuery("#chart" + id).append(msg + aform);
    });
  }
}

function sQuote(w) {
  return " '" + w + "' ";
}
